# Comando para actualizar la base de datos realizando el scaffold
Scaffold-DbContext -Provider Microsoft.EntityFrameworkCore.SqlServer -Connection "Data Source=localhost,14330;Initial Catalog=WebAPI;User Id=sa; Password=WebApi*2021!" -force

# Generar la migración y actualizar la base de datos.
- Se realiza en la consola de manejo de paquetes 
- Hay que tener seleccionado como poryecto de inicio NetCoreWebAPI
- Hay que tener seleccionado en la consola el proyecto por defecto DataAccessLayer
- En el contexto del EF, comentar la línea optionsBuilder.UseSqlServer();
- En el contexto del EF, descomentar la línea optionsBuilder.UseSqlServer("Data Source=localhost,14330;Initial Catalog=WebAPI;User Id=sa; Password=WebApi*2021!");
- Adaptar la Connection String a lo que corresponda
- Ejecutar: Add-Migration "<Mensaje de la migración>"
- Ejecutar: Update-Database

# Correr el proyecto con docker-compose
Hay que tener seleccionado por defecto el proyecto docker-compose y el sistema ejecutará los containers de la api y sql server.
Hay que tener seleccionado containers linux.
