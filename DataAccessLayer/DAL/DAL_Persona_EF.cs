﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DAL
{
    public class DAL_Persona_EF : IDAL.IDAL_Persona
    {
        private WebAPIContext _db;
        public DAL_Persona_EF(WebAPIContext db)
        {
            _db = db;
        }
        public Shared.ModeloDeDominio.Persona AddPersona(Shared.ModeloDeDominio.Persona x)
        {
            Persona p = new Persona()
            {
                Apellidos = x.Apellidos,
                Documento = x.Documento,
                Email = x.Email,
                FechaNacimiento = x.FechaNacimiento,
                Nombres = x.Nombres,
                Telefono = x.Telefono,
                TipoDocumento = x.TipoDocumento
            };

            _db.Personas.Add(p);
            _db.SaveChanges();
            x.Id = p.Id;
            return x;
        }

        public List<Shared.ModeloDeDominio.Persona> GetPersonas()
        {
            return _db.Personas.Select(x => new Shared.ModeloDeDominio.Persona()
            {
                Id = x.Id,
                Apellidos = x.Apellidos,
                Documento = x.Documento,
                Email = x.Email,
                FechaNacimiento = x.FechaNacimiento,
                Nombres = x.Nombres,
                Telefono = x.Telefono,
                TipoDocumento = x.TipoDocumento
            }).ToList();
        }
    }
}
