﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.IDAL
{
    public interface IDAL_Persona
    {
        Shared.ModeloDeDominio.Persona AddPersona(Shared.ModeloDeDominio.Persona x);
        List<Shared.ModeloDeDominio.Persona> GetPersonas();        
    }
}
