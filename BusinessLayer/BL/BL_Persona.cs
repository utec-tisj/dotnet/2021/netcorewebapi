﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public class BL_Persona : IBL.IBL_Persona
    {
        private readonly DataAccessLayer.IDAL.IDAL_Persona _dal;

        public BL_Persona(DataAccessLayer.IDAL.IDAL_Persona dal)
        {
            _dal = dal;
        }

        public Shared.ModeloDeDominio.Persona AddPersona(Shared.ModeloDeDominio.Persona x)
        {
            return _dal.AddPersona(x);
        }
        public List<Shared.ModeloDeDominio.Persona> GetPersonas()
        {
            return _dal.GetPersonas();
        }
    }
}
