﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreWebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "TEST")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly DataAccessLayer.WebAPIContext _db;

        public WeatherForecastController(DataAccessLayer.WebAPIContext db, ILogger<WeatherForecastController> logger)
        {
            _db = db;
            _logger = logger;
        }

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            DataAccessLayer.DAL.DAL_Persona_EF pef = new DataAccessLayer.DAL.DAL_Persona_EF(_db);
            pef.AddPersona(new Shared.ModeloDeDominio.Persona() { 
                Apellidos = "BAUZA SILVA",
                Documento = "45293204",
                Email = "cbauza@gmail.com",
                FechaNacimiento = new DateTime(1988,08,01),
                Nombres = "CRISTIAN ANDRES",
                Telefono = "099391218",
                TipoDocumento = "CI"            
            });
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
