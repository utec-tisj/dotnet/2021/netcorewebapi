﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NetCoreWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaController : ControllerBase
    {
        private readonly BusinessLayer.IBL.IBL_Persona _bl;

        public PersonaController(BusinessLayer.IBL.IBL_Persona bl)
        {
            _bl = bl;
        }

        // GET: api/<PersonaController>
        [HttpGet]
        public IEnumerable<Shared.ModeloDeDominio.Persona> Get()
        {
            return _bl.GetPersonas();
        }

        // POST api/<PersonaController>
        [HttpPost]
        public Shared.ModeloDeDominio.Persona Post([FromBody] Shared.ModeloDeDominio.Persona x)
        {
            return _bl.AddPersona(x);
        }
    }
}
